<?php
error_reporting( E_ALL );
require_once("SocketServer.class.php"); // Include the File
require_once("Candado.php"); // Include the File
$server = new SocketServer("93.188.164.49", 9999); // Create a Server binding to the given ip address and listen to port 31337 for connections
//$server = new SocketServer("192.168.0.62", 9999);
$server->max_clients = 200; // Allow no more than 10 people to connect at a time
$server->hook("CONNECT", "handle_connect"); // Run handle_connect every time someone connects
$server->hook("INPUT", "handle_input"); // Run handle_input whenever text is sent to the server


$server->infinite_loop(); // Run Server Code Until Process is terminated.

function handle_connect(&$server, &$candado, $input) {
    // SocketServer::socket_write_smart($candado,"String? ","");
   // echo "Nueva Conexion ";
}

function handle_input(&$server, &$candado, $input) {
    // $Lock  = "*CMDR,OM,863725031194523,000000000000,Q0,50#";
    // $unlock  = "*CMDR,OM,863725031194523,000000000000,Q0,50#";
    // $postion  = "*CMDR,OM,863725031194523,000000000000,Q0,50#";
    // $singIn  = *CMDR,OM,863725031194523,000000000000,Q0,50#
    // $status  = "*CMDR,OM,863725031194523,180427001530,S5,50,50,0,0,0#"  ;
    //  $RING  = "*CMDR,OM,863725031194523,000000000000,S8,0,0#";
    //$FIRMWARE  = "*CMDR,OM,863725031194523,000000000000,G0,v1.0,May 17 2017#";
    //$Alarm  = "*CMDR,OM,863725031194523,000000000000,W0,0#";
    //$ICCID  = "*CMDR,OM,863725031194523,000000000000,I0,800602B800110500000281#";
    
   // *CMDR,SH,12345,000000000000,D0,0,022030.00,A,0441.23869,N,07403.06124,W,10,0.89,051218,2592.4,M,A#
   //  *CMDR,SH,12345,000000000000,Q0,362#



    //$candado = new Candado( );
    // You probably want to sanitize your inputs here
    $trim = trim($input); // Trim the input, Remove Line Endings and Extra Whitespace.
    // $trim  = "*CMDR,DEVICECODE,IMEI,TIME,Q0,BATTERY,#"
   // echo "ClienteNumero: " . $candado;


    $comando = explode(",", $trim);

    $codeDevice = $comando[1];
    $IMEI = $comando[2];
    $candado->IMEI = $IMEI;
    $candado->timeConection = time();
    $server->deleteDuplicateLocks($candado);
    $lockTime = $comando[3];
    $command = $comando[4];
    date_default_timezone_set('America/Bogota');
    $time= "000000000000";
    $unlock = 0;
    $userID = 1020769546;
    $timeStamp = time();

    $inicioRespuesta = hexToStr('FF') . hexToStr('FF') . "*CMDS";
//  echo strToHex(ÿ); 

    $sp = ',';
    $finRespuesta = "#\n";
    mail("warningskloustrapps@gmail.com", "test", "paso IMEI =".$IMEI);
    mail("warningskloustrapps@gmail.com", "test", "paso command =".$command);
    switch ($command) {

        case 'L0':
            // Unlock
            $candado->unlock($IMEI, $comando[5], $comando[6], $comando[7]);
            $comandoRespuesta = 'Re,L0';
            $output = $inicioRespuesta . $sp . $codeDevice . $sp . $IMEI . $sp . $time . $sp . $comandoRespuesta . $finRespuesta;
            SocketServer::socket_write_smart($candado, $output); // Send the Client back the String
            break;

        case 'L1':
            //1. lock
            $candado->lock($IMEI, $comando[5], $comando[6], $comando[7]);
            $comandoRespuesta = 'Re,L1';
            $output = $inicioRespuesta . $sp . $codeDevice . $sp . $IMEI . $sp . $time . $sp . $comandoRespuesta . $finRespuesta;
            SocketServer::socket_write_smart($candado, $output); // Send the Client back the String 
            break;

        case 'D0':
            //4-GPS
            $candado->setPOS($comando[5] . ' - ' . $comando[6] . ' - ' . $comando[7] . ' - ' . $comando[8] . ' - ' .
                    $comando[9] . ' - ' . $comando[10] . ' - ' . $comando[11] . ' - ' . $comando[12] . ' - ' . $comando[13] .
                    ' - ' . $comando[14] . ' - ' . $comando[15] . ' - ' . $comando[16] . ' - ' . $comando[17]);

            $candado->setLatLng($comando[8], $comando[9], $comando[10], $comando[11], $IMEI);

            $comandoRespuesta = 'Re,D0';
            $output = $inicioRespuesta . $sp . $codeDevice . $sp . $IMEI . $sp . $time . $sp . $comandoRespuesta . $finRespuesta;
            SocketServer::socket_write_smart($candado, $output); // Send the Client back the String

            break;


        case 'Q0':
            //1. comando sing in
           
            $candado->singIn($codeDevice, $IMEI, $comando[5]);
            
           // $candado->IMEI = $IMEI;
            break;

        case 'H0':
            //1. Heartbeat
            $candado->Heartbeat($codeDevice, $IMEI, $comando[6], $comando[5], substr($comando[7], 0, 2));
            break;

        case 'S5':
            //5- STATUS
            $comandoRespuesta = 'S5';
            //$output = 'hola';
            $candado->setStatus($IMEI, $comando[5], $comando[6], $comando[7], $comando[8]);
           // $output = $inicioRespuesta . $sp . $codeDevice . $sp . $IMEI . $sp . $time . $sp . $comandoRespuesta . $finRespuesta;
           // SocketServer::socket_write_smart($candado, $output); // Send the Client back the String
            break;
        case 'S6':
            //5- STATUS
            $comandoRespuesta = 'Re,S5';
            $output = $inicioRespuesta . $sp . $codeDevice . $sp . $IMEI . $sp . $time . $sp . $comandoRespuesta . $finRespuesta;
            $candado->getStatus();
            break;
        case 'S8':
            //5- RINGING
            $candado->ringing($comando[5], $comando[6], $comando[7], $comando[8], $comando[9]);
            break;
        case 'G0':
            //2. FIRMWARE
            $candado->setFirmware($comando[5], $comando[6]);
            break;

        case 'W0':
            //2. ALARM
            $candado->alarm($comando[5]);
            break;

        case 'I0':
            //3-ICCID
            $candado->setICCID($comando[5]);
            break;
        case 'NO':
            break;
        default:
            SocketServer::socket_write_smart($candado, "Error\n");
            break;
    }



    if (strtolower($trim) == "quit") { // User Wants to quit the server
        SocketServer::socket_write_smart($candado, "Oh... Goodbye..."); // Give the user a sad goodbye message, meany!
        $server->disconnect($candado->server_clients_index); // Disconnect this client.
        return; // Ends the function
    }



//    SocketServer::socket_write_smart($candado,$output); // Send the Client back the String
    //SocketServer::socket_write_smart($candado,"String? ",""); // Request Another String
}

function pausa() {
    
}

function strToHex($string) {
    $hex = '';
    for ($i = 0; $i < strlen($string); $i++) {
        $ord = ord($string[$i]);
        $hexCode = dechex($ord);
        $hex .= substr('0' . $hexCode, -2);
    }
    return strToUpper($hex);
}

function hexToStr($hex) {
    $string = '';
    for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
        $string .= chr(hexdec($hex[$i] . $hex[$i + 1]));
    }
    return $string;
}
